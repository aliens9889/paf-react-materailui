import React from 'react';


// import React from 'react';
import {cyan500} from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';




const styles = {
    imageInput: {
        cursor: 'pointer',
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        width: '100%',
        opacity: 0,
    },
};

const style = {
    margin: 12,
};


// This replaces the textColor value on the palette
// and then update the keys for each component that depends on it.
// More on Colors: http://www.material-ui.com/#/customization/colors
const muiTheme = getMuiTheme({
    palette: {
        textColor: cyan500,
    },
    appBar: {
        height: 50,
    },
});


const FlatButtonExampleSimple = () => (
    <MuiThemeProvider muiTheme={muiTheme}>
        <div>
            <AppBar title="My AppBar" />
            <FlatButton label="Default" />
            <FlatButton label="Primary" primary={true} />
            <FlatButton label="Secondary" secondary={true} />
            <FlatButton label="Disabled" disabled={true} />

            <div>
                <FlatButton label="Choose an Image" labelPosition="before">
                    <input type="file" style={styles.imageInput} />
                </FlatButton>
                <FlatButton
                    label="Label before"
                    labelPosition="before"
                    primary={true}
                    style={styles.button}
                    icon={<ActionAndroid />}
                />
                <FlatButton
                    href="https://github.com/callemall/material-ui"
                    target="_blank"
                    label="GitHub Link"
                    secondary={true}
                    icon={<FontIcon className="muidocs-icon-custom-github" />}
                />
            </div>

            <div>
                <RaisedButton label="Default" style={style} />
                <RaisedButton label="Primary" primary={true} style={style} />
                <RaisedButton label="Secondary" secondary={true} style={style} />
                <RaisedButton label="Disabled" disabled={true} style={style} />
                <br />
                <br />
                <RaisedButton label="Full width" fullWidth={true} />
            </div>

            <div>
                <FloatingActionButton style={style}>
                    <ContentAdd />
                </FloatingActionButton>
                <FloatingActionButton mini={true} style={style}>
                    <ContentAdd />
                </FloatingActionButton>
                <FloatingActionButton secondary={true} style={style}>
                    <ContentAdd />
                </FloatingActionButton>
                <FloatingActionButton mini={true} secondary={true} style={style}>
                    <ContentAdd />
                </FloatingActionButton>
                <FloatingActionButton disabled={true} style={style}>
                    <ContentAdd />
                </FloatingActionButton>
                <FloatingActionButton mini={true} disabled={true} style={style}>
                    <ContentAdd />
                </FloatingActionButton>
            </div>

            <Card>
                <CardHeader
                    title="URL Avatar"
                    subtitle="Subtitle"
                    avatar="https://s-media-cache-ak0.pinimg.com/564x/c1/e1/49/c1e149e9f7f036b27b02807482d6ed5c.jpg"
                />
                <CardMedia
                    overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
                >
                    <img src="https://s-media-cache-ak0.pinimg.com/564x/c1/e1/49/c1e149e9f7f036b27b02807482d6ed5c.jpg" />
                </CardMedia>
                <CardTitle title="Card title" subtitle="Card subtitle" />
                <CardText>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                    Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                    Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                </CardText>
                <CardActions>
                    <FlatButton label="Action1" />
                    <FlatButton label="Action2" />
                </CardActions>
            </Card>

        </div>
    </MuiThemeProvider>
);

export default FlatButtonExampleSimple;



// class Example extends React.Component {
//     render () {
//         return (
//             <div className="example">
//                 <h1>Hola Mundo</h1>
//                 <FlatButtonExampleSimple/>
//             </div>
//         );
//     }
// }
//
// export default Example;

