import React from 'react';
import TextField from 'material-ui/TextField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AutoComplete from 'material-ui/AutoComplete';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const sucursales = [
    'Merida',
    'Zulia',
    'Lara',
    'Tachira',
    'Guarico'
];


const styles = {

    Paper: {
        margin: 20,
        padding: 100,
    },

    Button: {
        margin: 12
    }

};

const PafInvoice = () => (
    <MuiThemeProvider>
        <div>
            <form action="">
                <Paper zDepth={3} style={styles.Paper}>
                    <div>
                        <a href="#">Consulta Historico</a>
                    </div>

                    <h1>Solicitud de Factura</h1>
                    <AutoComplete
                        floatingLabelText="Sucursal *"
                        hintText="Ingrese sucursal"
                        filter={AutoComplete.caseInsensitiveFilter}
                        dataSource={sucursales}
                        fullWidth={true}
                    /><br />
                    <TextField
                        hintText="Ej: 134.00"
                        floatingLabelText="Monto Ticket *"
                        floatingLabelFixed={false}
                        fullWidth={true}
                    /><br />
                    <TextField
                        hintText="Ej: 2344567"
                        floatingLabelText="Ticket Id *"
                        floatingLabelFixed={false}
                        fullWidth={true}
                    /><br />
                    <TextField
                        hintText="Ej: 2344567"
                        floatingLabelText="R.F.C."
                        floatingLabelFixed={false}
                        fullWidth={true}
                    /><br />
                    <p>Si tiene alguna duda o problema para generar su factura, favor de enviar un correo a
                        facturacion@carlogio.com, incluyendo ticket digitalizado y los datos fiscales de facturación
                        completos</p>
                    <div>
                        <RaisedButton
                            label="Siguiente"
                            primary={true}
                            fullWidth={true}
                        />
                    </div>
                    <div>
                        <p>Los campos con * son requeridos para poder acceder a generar la factura.</p>
                    </div>


                </Paper>
            </form>
        </div>
    </MuiThemeProvider>

);

export default PafInvoice;