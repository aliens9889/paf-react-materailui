import React from 'react';
import AppBar from 'material-ui/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


const PafHeader = () => (
    <MuiThemeProvider>
        <AppBar
            title="Facturacion Electronica"
        />
    </MuiThemeProvider>

);

export default PafHeader;