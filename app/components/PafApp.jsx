import React from 'react';


import Example from 'Example';
import Test from 'Test';
import PafInvoice from 'PafInvoice';
import PafHeader from 'PafHeader';
import PafFooter from 'PafFooter';

class PafApp extends React.Component {

    render() {
        return (
            <div className="container">
                {/*<Example/>*/}
                {/*<Test/>*/}
                <PafHeader/>
                <PafInvoice/>
                <PafFooter/>
            </div>
        );
    }

}

module.exports = PafApp;