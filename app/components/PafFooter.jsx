import React from 'react';
import AppBar from 'material-ui/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


const PafHeader = () => (
    <MuiThemeProvider>
        <AppBar
            title="Facturación Electrónica | Todos los Derechos Reservados Ambit Inc. 2015 | Licencias | Políticas de Privacidad"
        />
    </MuiThemeProvider>

);

export default PafHeader;