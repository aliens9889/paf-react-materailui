import React from 'react';
import ReactDOM from 'react-dom';

import router from 'app/router/';

import PafApp from 'PafApp';

import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

// App css
require('style!css!sass!applicationStyles')

ReactDOM.render(<PafApp/>, document.getElementById('app'));